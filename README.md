# Mercury
Mecury is an incredible tool that might one day save the world. Or destroy it. As the information it displays is randomized, it's kind of difficult to say for sure. The best bet is to forget it and not to launch it to often, just in case.

Mercury has two modes: **demo** and **dev**. Both of them use Docker and docker-compose.

# REQUIREMENTS
- `Python 3.8`
- `backend/requirements.txt`

# DEMO mode

**ONLY FOR DEMO PURPOSES, DO NOT USE IT IN PRODUCTION AS IS.**

Demo mode aims to get as close as possible to production condition :
- frontend: 
  - staticfiles delivered by `nginx` ;
  - the `./backend/` folder is copied inside the `backend` image during the `build` process to `/opt/django_project`;
- backend: 
  - delivered by `daphne` (asgi server) and accessed through `nginx` (as reverse proxy);
  - the `./frontend/dist/` folder is copied inside the `frontend` image during the `build` process to `/opt/vuejs_project`;


To start Mercury in demo mode, run the following command in the project's root folder:
`./run_demo.sh`

The website is exposed by docker on port 8888 of the localhost : http://127.0.0.1:8888


```mermaid
graph TB
    F1[nginx] -- reverse proxy /api/ --> B1
    S1[script] -- create Temperature 1/s --> B1
    subgraph "FRONTEND"
    F1[nginx:8080] -- serve / --> F2
    F2[/"/opt/vuejs_project/dist/"/]
    end
    subgraph "USER"
    U1[browser] -- http --> F1
    end
    subgraph "SENSOR"
    S1
    end
    subgraph "BACKEND"
    B1[daphne:8080] -- serve /api/ --> B2
    B2[/"/opt/django_project/"/]
    end
```

# DEV mode
Dev mode aims at facilitating developpement effort (hot-reload, etc):
- frontend: 
  - delivered by `npm run serve` through `nginx` (as reverse proxy);
  - the `backend` container get the `./backend/` folder mounted as a volume  in `/opt/django_project`; 
- backend: 
  - django's `runserver` through `nginx` (as reverse proxy);
  - the `frontend` container get the `./frontend/` folder mounted as a volume in `/opt/vuejs_project`;

To start Mercury in demo mode, run the following command in the project's root folder:
`./run_dev.sh`

The website is exposed by docker on port 8888 of the localhost : http://127.0.0.1:8888

```mermaid
graph TB
    F1 -- reverse proxy /api/ --> B1
    F1 -- reverse proxy / --> F2
    S1 -- create Temperature 1/s --> B1   
    U1 -- http --> F1 

    subgraph "FRONTEND"
    F1[nginx:8080]
    F2["npm run serve :8081"] -- serve / --> F3
    F3[/"/opt/vuejs_project/dist/"/]
    end
    subgraph "USER"
    U1[browser]
    end
    subgraph "SENSOR"
    S1[script]
    end
    subgraph "BACKEND"
    B1[manage.py runserver :8080] -- serve /api/ --> B2
    B2[/"/opt/django_project/"/]
    end


```
# GRAPHQL Console
If you want to access the `graphene-django` grphql console, it is accessible there:
http://127.0.0.1:8888/api/graphql/

# Test
## backend
Test in Mercury are written with pytest. In order to launch the test sequence: 
```bash
cd ./backend
python3.8 -m pip install -r ./requirements.txt
python3.8 -m pip install -r ./requirements_dev.txt
pytest
```

To generate and display the coverge report: 
```bash
cd ./backend
python3.8 -m pip install -r ./requirements.txt
python3.8 -m pip install -r ./requirements_dev.txt
coverage run -m pytest
coverage report
```

## frontend
@TODO no frontend code coverage yet