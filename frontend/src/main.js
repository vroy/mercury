import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false


import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { split } from 'apollo-link'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'



const httpLink = createHttpLink({
    uri: 'http://127.0.0.1:8888/api/graphql/',
})
const wsLink = new WebSocketLink({
    uri: 'ws://127.0.0.1:8888/api/graphql/',
    options: {
        reconnect: true,
    },
})
const link = split(
    // split based on operation type
    ({ query }) => {
        const definition = getMainDefinition(query)
        return definition.kind === 'OperationDefinition' &&
            definition.operation === 'subscription'
    },
    wsLink,
    httpLink
)

// Cache implementation
const cache = new InMemoryCache()

// Create the apollo client
const apolloClient = new ApolloClient({
    link: link,
    // link: httpLink,
    cache: cache,
})

const apolloProvider = new VueApollo({
    defaultClient: apolloClient,
})

import VueApollo from 'vue-apollo'
Vue.use(VueApollo)
new Vue({
    el: '#app',
    apolloProvider: apolloProvider,
    render: h => h(App),
})