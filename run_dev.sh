#!/bin/bash
docker build -t mercury_backend -f dockerfiles/mercury_backend/Dockerfile ./
docker build -t mercury_sensor -f dockerfiles/mercury_sensor/Dockerfile ./
docker build -t mercury_frontend -f dockerfiles/mercury_frontend/Dockerfile ./
docker-compose -f dockerfiles/docker-compose.yml -f dockerfiles/docker-compose.dev.yml up