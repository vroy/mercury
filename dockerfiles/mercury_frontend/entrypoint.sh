#!/bin/bash
mode=${1:-demo}
if [ "$mode" == "dev" ]; then
    echo "now running 'npm install' command";
    nginx -c /etc/nginx/nginx.dev.conf & 
    npm install
    npm run serve
else
    nginx
fi