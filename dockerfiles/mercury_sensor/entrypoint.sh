#!/bin/sh
while true
do
    echo "Creating new temperature measurement using curl on graphql mutation: 'createTemperature'";
    now=$(date +%s);
    value=$(( RANDOM % 40 -10));
    query="mutation{createTemperature(input:{timestamp: ${now}, unit:\\\"C\\\", value: ${value}}) { temperature {id timestamp value unit} clientMutationId}}"
    curl \
    -X POST \
    -H "Content-Type: application/json" \
    --data "{\"query\": \"$query\"}" \
    http://backend:8080/api/graphql/ > /dev/null 2>&1;
    sleep 1;
done
