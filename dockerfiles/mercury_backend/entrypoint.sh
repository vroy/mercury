#!/bin/bash
mode=${1:-demo} 

if [ -f "/etc/mercury.json" ]
then
    echo "/etc/mercury.json already exists."
else
    DJANGO_SECRET_KEY=$(/opt/venvs/django_project/bin/python3.8 -c 'from django.core.management import utils; print(utils.get_random_secret_key())')                                         
    echo "{\"DJANGO_SECRET_KEY\":\"$DJANGO_SECRET_KEY\"}"> /etc/mercury.json
fi


/opt/venvs/django_project/bin/python3.8 /opt/django_project/manage.py migrate --no-input

if [ "$mode" == "dev" ]; then
    /opt/venvs/django_project/bin/python3.8 /opt/django_project/manage.py runserver 0.0.0.0:8080
else
    /opt/venvs/django_project/bin/daphne -b 0.0.0.0 -p 8080 django_project.asgi:application
fi