from django.urls import path
from channels.routing import ProtocolTypeRouter, URLRouter
from django_project.schema import MyGraphqlWsConsumer


application = ProtocolTypeRouter(
    {
        "websocket": URLRouter(
            [
                path("api/graphql/", MyGraphqlWsConsumer),
            ]
        )
    }
)
