import graphene
import channels_graphql_ws

from mercury.query import Query as MercuryQuery
from mercury.mutation import Mutation as MercuryMutation
from mercury.subscription import Subscription as MercurySubscription


class Query(MercuryQuery, graphene.ObjectType):
    pass


class Mutation(MercuryMutation, graphene.ObjectType):
    pass


class Subscription(MercurySubscription, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation, subscription=Subscription)


class MyGraphqlWsConsumer(channels_graphql_ws.GraphqlWsConsumer):
    schema = schema
