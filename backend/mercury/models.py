from django.db import models


class Temperature(models.Model):
    value = models.IntegerField()
    unit = models.CharField(max_length=1)
    timestamp = models.IntegerField()
