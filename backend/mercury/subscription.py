import graphene
import channels_graphql_ws

from mercury.types import TemperatureType


class CurrentTemperatureSubscribe(channels_graphql_ws.Subscription):
    temperature = graphene.Field(TemperatureType)

    @staticmethod
    def publish(payload, info):
        return CurrentTemperatureSubscribe(temperature=payload)


class Subscription(graphene.ObjectType):
    current_temperature_subscribe = CurrentTemperatureSubscribe.Field()
