import graphene
from graphene_django.forms.mutation import DjangoModelFormMutation

from mercury.types import TemperatureType
from mercury.forms import TemperatureForm


class TemperatureMutation(DjangoModelFormMutation):
    temperature = graphene.Field(TemperatureType)

    class Meta:
        form_class = TemperatureForm


class Mutation(graphene.ObjectType):
    create_temperature = TemperatureMutation.Field()
