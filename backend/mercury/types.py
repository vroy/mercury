from graphene_django import DjangoObjectType
from mercury.models import Temperature


class TemperatureType(DjangoObjectType):
    class Meta:
        model = Temperature
        fields = "__all__"
