from django.db.models.signals import post_save
from django.dispatch import receiver

from mercury.models import Temperature
from mercury.subscription import CurrentTemperatureSubscribe


@receiver(post_save, sender=Temperature)
def broadcast_current_temperature(sender, **kwargs):
    temperature = Temperature.objects.all().order_by("-timestamp").first()
    CurrentTemperatureSubscribe.broadcast(payload=temperature)
