from django import forms
from mercury.models import Temperature


class TemperatureForm(forms.ModelForm):
    class Meta:
        model = Temperature
        fields = "__all__"
