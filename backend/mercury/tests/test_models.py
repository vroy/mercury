import pytest
from mercury.models import Temperature


@pytest.mark.django_db
def test_temperature_create():
    Temperature.objects.create(timestamp=0, value=42, unit="C")
    assert Temperature.objects.count() == 1
