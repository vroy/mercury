import uuid

import graphene
import pytest
import channels
import django

import channels_graphql_ws
from channels_graphql_ws.testing import GraphqlWsTransport, GraphqlWsClient
from django_project.schema import MyGraphqlWsConsumer
from django_project import routing


@pytest.fixture
def gql(db, request):
    del db
    issued_clients = []

    def client_constructor():
        MyGraphqlWsConsumer.confirm_subscriptions = True
        MyGraphqlWsConsumer.strict_ordering = True

        transport = GraphqlWsTransport(
            application=routing.application,
            path="api/graphql/",
        )
        client = GraphqlWsClient(transport)
        issued_clients.append(client)
        return client

    yield client_constructor

    for client in reversed(issued_clients):
        assert (
            not client.connected
        ), f"Test has left connected client: {request.node.nodeid}!"


@pytest.mark.asyncio
async def test_confirmation_enabled(gql):
    client = gql()

    await client.connect_and_init()
    sub_op_id = await client.send(
        msg_type="start",
        payload={
            "query": """
                subscription
                {
                    currentTemperatureSubscribe
                    {
                        temperature
                        {
                            timestamp
                            value
                            unit
                        }
                    }
                }
            """,
        },
    )

    resp = await client.receive(assert_id=sub_op_id, assert_type="data")
    assert resp == {"data": None}
    mut_op_id = await client.send(
        msg_type="start",
        payload={
            "query": """
                mutation{
                createTemperature(input:
                    {
                        timestamp: 42,
                        unit:"C",
                        value: 110
                    }) {
                        temperature {
                            id
                            timestamp
                            value
                            unit
                        }
                        clientMutationId
                    }
                }
            """,
        },
    )
    await client.receive(assert_id=mut_op_id, assert_type="data")
    await client.receive(assert_id=mut_op_id, assert_type="complete")
    resp = await client.receive(assert_id=sub_op_id, assert_type="data")
    assert resp["data"]["currentTemperatureSubscribe"]["temperature"]["timestamp"] == 42
    await client.assert_no_messages(
        "Unexpected message received at the end of the test!"
    )
    await client.finalize()
