import json
import pytest
from graphene_django.utils.testing import graphql_query
from mercury.models import Temperature


@pytest.fixture
def client_query(client):
    def func(*args, **kwargs):
        return graphql_query(*args, **kwargs, client=client)

    return func


# Test you query using the client_query fixture
@pytest.mark.django_db
def test_current_temperature_query(client_query):
    Temperature.objects.create(timestamp=0, value=42, unit="C")
    Temperature.objects.create(timestamp=1000, value=42, unit="C")
    Temperature.objects.create(timestamp=500, value=42, unit="C")
    response = client_query(
        """
        query
        {
            currentTemperature
            {
                timestamp
                value
                unit
            }
        }
        """,
        graphql_url="/api/graphql/",
    )

    content = json.loads(response.content)
    assert "errors" not in content
    assert content["data"]["currentTemperature"]["timestamp"] == 1000
