import json
import pytest
from graphene_django.utils.testing import graphql_query
from mercury.models import Temperature


@pytest.fixture
def client_query(client):
    def func(*args, **kwargs):
        return graphql_query(*args, **kwargs, client=client)

    return func


# Test you query using the client_query fixture
@pytest.mark.django_db
def test_create_temperature_mutation(client_query):
    response = client_query(
        """
            mutation{
            createTemperature(input:
                {
                    timestamp: 42,
                    unit:"C",
                    value: 110
                }) {
                    temperature {
                        id
                        timestamp
                        value
                        unit
                    }
                    clientMutationId
                }
            }
        """,
        graphql_url="/api/graphql/",
    )

    content = json.loads(response.content)
    assert "errors" not in content
    assert content["data"]["createTemperature"]["temperature"]["timestamp"] == 42
    currentTemperature = Temperature.objects.order_by("-timestamp").first()
    assert currentTemperature.timestamp == 42
