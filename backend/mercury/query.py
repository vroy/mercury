import graphene

from mercury.types import TemperatureType
from mercury.models import Temperature


class Query(graphene.ObjectType):
    current_temperature = graphene.Field(TemperatureType)

    def resolve_current_temperature(root, info):
        return Temperature.objects.order_by("-timestamp").first()
