from django.apps import AppConfig


class MercuryConfig(AppConfig):
    name = "mercury"

    def ready(self):
        import mercury.signals  # noqa
